import * as sapper from '@sapper/app';
import { getCustomers, getSites } from './helpers.js';

sapper.start({
	target: document.querySelector('#sapper')
}).then(() => {
	console.log('Start App');
	getCustomers();
	getSites();
});

