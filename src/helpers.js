import {allCustomers, codeCustomers, allSites} from "./routes/stores";

export function getCustomers() {
    fetch('http://localhost:8080/v1/customer/list',
        {method: 'post'})
        .then((response) => response.json())
        .then((res) => {
            storeCustomersValue(res.customerResponse);
            console.log(res.customerResponse);
            return {customers: res.customerResponse}
        }).catch((error) => {
        console.error('Error:', error);
    });
}

function storeCustomersValue(customers) {
    let items = [];
    for(let i = 0; i < customers.length; i++){
        items.push({value: customers[i].id, label:  customers[i].code})
    }
    allCustomers.set(customers);
    codeCustomers.set(items);
}

export function getSites() {
    fetch('http://localhost:8080/v1/site/list',
        {method: 'post'})
        .then((response) => response.json())
        .then((res) => {
            storeSitesValue(res.siteResponse);
            console.log(res.siteResponse);
            return { customers: res.siteResponse }
        }).catch((error) => {
        console.error('Error:', error);
    });
}

function storeSitesValue(sites) {
    allSites.set(sites);
}